package co.edu.unbosque.view;
import java.awt.*;
import javax.swing.*;
public class FondoCalculadora extends JPanel {
    public void paint(Graphics g) {
        Image fondo = new ImageIcon("src/fondo/background.png").getImage();
        g.drawImage(fondo, 0, 0, getWidth(), getHeight(), this);
        setOpaque(false);
        super.paint(g);
    }
}
