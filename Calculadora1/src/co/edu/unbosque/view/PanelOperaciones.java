package co.edu.unbosque.view;

import java.awt.Color;
import java.awt.Font;
import java.awt.GridLayout;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.border.Border;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

public class PanelOperaciones extends JPanel{

	private JButton bBinAdeci;
	private JButton bBinAhex;
	private JButton bDeciAbin;
	private JButton bDeciAhex;
	private JButton bHexAbin;
	private JButton bHexAdeci;
	private JButton bSumBin;
	private JButton bOtros;
	private JButton bVolver;
	
	
	
	public PanelOperaciones() {
		setLayout(null);
		inicializarComponentes();
		setVisible(true);
		setOpaque(false);
	}
	
	public void inicializarComponentes() {
		Border border = new LineBorder(new Color(0,0,0), 0, true);
		TitledBorder tb = new TitledBorder(border, "Panel de operaciones");
		Font fuente = new Font("Tahoma", Font.BOLD, 12);
		tb.setTitleFont(fuente);
		tb.setTitleColor(Color.WHITE);
		setBorder(tb);
		
		bBinAdeci = new JButton("Binario a Decimal");
		bBinAdeci.setBounds(25, 30, 170, 20);
		add(bBinAdeci);
		bBinAdeci.setActionCommand("BINADECI");
		 
		bBinAhex = new JButton("Binario a Hexadecimal");
		bBinAhex.setBounds(25, 75, 170, 20);
		add(bBinAhex);
		bBinAhex.setActionCommand("BINAHEX");
		
		 
		bDeciAbin = new JButton("Decimal a Binario");
		bDeciAbin.setBounds(225, 30, 170, 20);
		add(bDeciAbin);
		bDeciAbin.setActionCommand("DECIABIN");
		 
		bDeciAhex = new JButton("Decimal a Hexadecimal"); 
		bDeciAhex.setBounds(225, 75, 170, 20);
		add(bDeciAhex);
		bDeciAhex.setActionCommand("DECIAHEX");
		 
		
		
		bHexAbin = new JButton("Hexadecimal a Binario");
		add(bHexAbin);
		bHexAbin.setBounds(415, 30, 170, 20);
		bHexAbin.setActionCommand("HEXAABIN");
		 
		bHexAdeci = new JButton("Hexadecimal a Decimal");
		add(bHexAdeci);
		bHexAdeci.setBounds(415, 75, 170, 20);
		bHexAdeci.setActionCommand("HEXAADECI");
		
		
		bSumBin = new JButton("Suma de binarios"); 
		bSumBin.setBounds(225, 120, 170, 20);
		bSumBin.setVisible(false);
		add(bSumBin);
		bSumBin.setActionCommand("SUMBIN");
		
		bOtros = new JButton("Otras Operaciones"); 
		bOtros.setBounds(415, 120, 170, 20);
		bOtros.setVisible(true);
		setVisible(false);
		add(bOtros);
		bOtros.setActionCommand("OTROS");
		
		bVolver = new JButton("Volver"); 
		bVolver.setBounds(25, 120, 170, 20);
		bVolver.setVisible(false);
		setVisible(false);
		add(bVolver);
		bVolver.setActionCommand("VOLVER");
		
		
		
	}
	

	public JButton getbVolver() {
		return bVolver;
	}

	public JButton getbBinAdeci() {
		return bBinAdeci;
	}

	public JButton getbBinAhex() {
		return bBinAhex;
	}

	public JButton getbDeciAbin() {
		return bDeciAbin;
	}

	public JButton getbDeciAhex() {
		return bDeciAhex;
	}

	public JButton getbHexAbin() {
		return bHexAbin;
	}

	public JButton getbHexAdeci() {
		return bHexAdeci;
	}

	public JButton getbSumBin() {
		return bSumBin;
	}

	public JButton getbOtros() {
		return bOtros;
	}
	

}
