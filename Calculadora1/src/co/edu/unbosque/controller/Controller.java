package co.edu.unbosque.controller;
import co.edu.unbosque.model.*;
import co.edu.unbosque.view.VentanaPrincipal;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.util.*;

import javax.swing.JOptionPane;

public class Controller implements ActionListener {
	private Funciones funcion;
	private VentanaPrincipal ventana;

	public Controller() {
		ventana = new VentanaPrincipal();
		funcion = new Funciones();
		asignarOyentes();
	}
	public void asignarOyentes() {
		ventana.getPoperaciones().getbBinAdeci().addActionListener(this);
		ventana.getPoperaciones().getbBinAhex().addActionListener(this);
		ventana.getPoperaciones().getbDeciAbin().addActionListener(this);
		ventana.getPoperaciones().getbDeciAhex().addActionListener(this);
		ventana.getPoperaciones().getbHexAbin().addActionListener(this);
		ventana.getPoperaciones().getbHexAdeci().addActionListener(this);
		ventana.getPoperaciones().getbSumBin().addActionListener(this);
		ventana.getPoperaciones().getbOtros().addActionListener(this);
		ventana.getPoperaciones().getbVolver().addActionListener(this);
		
		
		

	}
	public void actionPerformed(ActionEvent e ) {
		String comando = e.getActionCommand();
		String aux = "";
		String aux2 = "";
		long aux_long = 0;
		String aux_string = "";
		String aux_string2 = "";
		switch(comando) {
			case "BINADECI":
				aux = ventana.getPdatos().getCnum1().getText();
				aux_string = String.valueOf(aux);
				if(funcion.validarBinario(aux_string)) {
					ventana.getPrespuesta().getErta().setText("El resultado es: "+funcion.binarioADecimal(aux_string));
					break;
				}else {
					JOptionPane.showMessageDialog(null, "Error: número invalido (rango entre 0 y 1) ", "Error", JOptionPane.ERROR_MESSAGE);
					break;
				}
				
			
			case "BINAHEX":
				aux = ventana.getPdatos().getCnum1().getText();
				aux_string = String.valueOf(aux);
				if(funcion.validarBinario(aux_string)) {
					ventana.getPrespuesta().getErta().setText("El resultado es: "+funcion.binarioAHexadecimal(aux_string));
					break;
				}else {
					JOptionPane.showMessageDialog(null, "Error: número invalido (rango entre 0 y 1) ", "Error", JOptionPane.ERROR_MESSAGE);
					break;
				}
				
				
			
			case "DECIAHEX":
				aux = ventana.getPdatos().getCnum1().getText();
				try {
					aux_long = Long.parseLong(aux);
					ventana.getPrespuesta().getErta().setText("El resultado es: "+funcion.decimalAHexadecimal(aux_long));
					break;
				}catch (NumberFormatException nfe) {
					JOptionPane.showMessageDialog(null, "Error: número invalido (número no entero)", "Error", JOptionPane.ERROR_MESSAGE);
					break;
				}
				
					
				
			
			case "DECIABIN":
				aux = ventana.getPdatos().getCnum1().getText();
				try {
					aux_long = Long.parseLong(aux);
					ventana.getPrespuesta().getErta().setText("El resultado es: "+funcion.decimalABinario(aux_long));
					break;
				}catch (NumberFormatException nfe) {
					JOptionPane.showMessageDialog(null, "Error: número invalido (número no entero)", "Error", JOptionPane.ERROR_MESSAGE);
					break;
				}
				
				
				
			
			case "HEXAABIN":
				aux = ventana.getPdatos().getCnum1().getText();
				aux_string = String.valueOf(aux);
				if(funcion.validarHexadecimal(aux_string)) {
					ventana.getPrespuesta().getErta().setText("El resultado es: "+funcion.hexadecimalABinario(aux_string));
					break;
				}else if(funcion.validarHexadecimal(aux_string)){
					JOptionPane.showMessageDialog(null, "Error: número invalido [rango de 0 a 9 y A(10) a F(15)]", "Error", JOptionPane.ERROR_MESSAGE);
					break;
				}
				
			
			case "HEXAADECI":
				aux = ventana.getPdatos().getCnum1().getText();
				aux_string = String.valueOf(aux);
				if(funcion.validarHexadecimal(aux_string)) {
					ventana.getPrespuesta().getErta().setText("El resultado es: "+funcion.hexadecimalADecimal(aux_string));
					break;
				}else {
					JOptionPane.showMessageDialog(null, "Error: número invalido [rango de 0 a 9 y A(10) a F(15)]", "Error", JOptionPane.ERROR_MESSAGE);
					break;
				}
			case "OTROS":
				ventana.getPoperaciones().getbBinAdeci().setVisible(false);
				ventana.getPoperaciones().getbBinAhex().setVisible(false);
				ventana.getPoperaciones().getbDeciAbin().setVisible(false);
				ventana.getPoperaciones().getbDeciAhex().setVisible(false);
				ventana.getPoperaciones().getbHexAbin().setVisible(false);
				ventana.getPoperaciones().getbHexAdeci().setVisible(false);
				ventana.getPoperaciones().getbOtros().setVisible(false);
				ventana.getPoperaciones().getbSumBin().setVisible(true);
				ventana.getPoperaciones().getbVolver().setVisible(true);
				ventana.getPrespuesta().getErta().setText("");
				ventana.getPdatos().getCnum2().setVisible(true);
				ventana.getPdatos().getEnum2().setVisible(true);
				
				break;
			
			case "SUMBIN":
				aux = ventana.getPdatos().getCnum1().getText();
				aux2 = ventana.getPdatos().getCnum2().getText();
				aux_string = String.valueOf(aux);
				aux_string2 = String.valueOf(aux2);
				if(funcion.validarBinario(aux_string) && funcion.validarBinario(aux_string2)) {
					ventana.getPrespuesta().getErta().setText("El resultado es: "+funcion.sumaBinarios(aux_string, aux_string2));
					break;
				}else {
					JOptionPane.showMessageDialog(null, "Error: número invalido (rango entre 0 y 1) ", "Error", JOptionPane.ERROR_MESSAGE);
					break;
				}
				
			case "VOLVER":
				ventana.getPoperaciones().getbBinAdeci().setVisible(true);
				ventana.getPoperaciones().getbBinAhex().setVisible(true);
				ventana.getPoperaciones().getbDeciAbin().setVisible(true);
				ventana.getPoperaciones().getbDeciAhex().setVisible(true);
				ventana.getPoperaciones().getbHexAbin().setVisible(true);
				ventana.getPoperaciones().getbHexAdeci().setVisible(true);
				ventana.getPoperaciones().getbOtros().setVisible(true);
				ventana.getPoperaciones().getbSumBin().setVisible(false);
				ventana.getPoperaciones().getbVolver().setVisible(false);
				ventana.getPrespuesta().getErta().setText("");
				ventana.getPdatos().getCnum2().setVisible(false);
				ventana.getPdatos().getEnum2().setVisible(false);
				break;
				
		}
		
	}
}